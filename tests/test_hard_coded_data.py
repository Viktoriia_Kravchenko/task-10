import pytest
from myapp import *

app.testing = True


@pytest.fixture()
def client():
    return app.test_client()


def test_smaller_and_equal_groups(client):
    response = client.get("/api/v1/groups/students")

    assert response.status_code == 200
    assert "application/json" == response.headers["Content-Type"]
    assert response.json == {
        "1": {"name": "DF-33"},
        "2": {"name": "HH-67"},
        "3": {"name": "DP-11"}
    }


def test_course_students(client):
    response = client.get("/api/v1/courses/biology/students")

    assert response.status_code == 200
    assert "application/json" == response.headers["Content-Type"]
    assert response.json == {
        "1": {"first_name": "Mina", "last_name": "Shevon", "group_id": 1},
        "2": {"first_name": "Rosabella", "last_name": "Krysten", "group_id": 2},
        "3": {"first_name": "Carlene", "last_name": "Freda", "group_id": 3},
        "4": {"first_name": "Martina", "last_name": "Cecelia", "group_id": 1},
        "5": {"first_name": "Kacie", "last_name": "Elma", "group_id": 2},
        "6": {"first_name": "Nikki", "last_name": "Ellington", "group_id": 3}
    }


def test_new_student(client):
    response = client.post("/api/v1/students/7", json={
        "first_name": "Bill", "last_name": "Brown", "group_id": 1
    })

    assert response.status_code == 200
    assert "application/json" == response.headers["Content-Type"]
    assert response.json == {"first_name": "Bill", "last_name": "Brown", "group_id": 1}


def test_delete_student(client):
    response = client.delete("/api/v1/students/6", json={})

    assert response.status_code == 200
    assert "application/json" == response.headers["Content-Type"]
    assert response.json == {
        "1": {"first_name": "Mina", "last_name": "Shevon", "group_id": 1},
        "2": {"first_name": "Rosabella", "last_name": "Krysten", "group_id": 2},
        "3": {"first_name": "Carlene", "last_name": "Freda", "group_id": 3},
        "4": {"first_name": "Martina", "last_name": "Cecelia", "group_id": 1},
        "5": {"first_name": "Kacie", "last_name": "Elma", "group_id": 2},
        "7": {"first_name": "Bill", "last_name": "Brown", "group_id": 1}
    }


def test_add_student_to_course(client):
    response = client.post("/api/v1/students/5/3", json={
        "student_id": 5, "course_id": 3
    })

    assert response.status_code == 200
    assert "application/json" == response.headers["Content-Type"]
    assert response.json == {
        "student_id": 5, "course_id": 3
    }


def test_remove_student_from_course(client):
    response = client.delete("/api/v1/students/1", json={})

    assert response.status_code == 200
    assert "application/json" == response.headers["Content-Type"]
    assert response.json == {
        "2": {"first_name": "Rosabella", "last_name": "Krysten", "group_id": 2},
        "3": {"first_name": "Carlene", "last_name": "Freda", "group_id": 3},
        "4": {"first_name": "Martina", "last_name": "Cecelia", "group_id": 1},
        "5": {"first_name": "Kacie", "last_name": "Elma", "group_id": 2},
        "7": {"first_name": "Bill", "last_name": "Brown", "group_id": 1}
    }

