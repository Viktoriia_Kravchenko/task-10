from flask import Flask
from flask_restful import Resource, Api, abort, reqparse

app = Flask(__name__)
api = Api(app)

GROUPS = {
    1: {"name": "DF-33"},
    2: {"name": "HH-67"},
    3: {"name": "DP-11"}
}
STUDENTS = {
    1: {"first_name": "Mina", "last_name": "Shevon", "group_id": 1},
    2: {"first_name": "Rosabella", "last_name": "Krysten", "group_id": 2},
    3: {"first_name": "Carlene", "last_name": "Freda", "group_id": 3},
    4: {"first_name": "Martina", "last_name": "Cecelia", "group_id": 1},
    5: {"first_name": "Kacie", "last_name": "Elma", "group_id": 2},
    6: {"first_name": "Nikki", "last_name": "Ellington", "group_id": 3}
}
COURSES = {
    1: {"name": "math", "description": "we develop algebraic concepts and skills needed to graph and solve linear "
                                       "equations and inequalities"},
    2: {"name": "biology", "description": "this course includes a study of living organisms and vital processes"},
    3: {"name": "informatics", "description": "here we speak about natural and engineered computational systems"},
    4: {"name": "history", "description": "study the past and make conclusions"}
}

ASSOCIATIONS = {
    1: {"student_id": 1, "course_id": 1},
    2: {"student_id": 2, "course_id": 2},
    3: {"student_id": 3, "course_id": 3},
    4: {"student_id": 4, "course_id": 4},
    5: {"student_id": 5, "course_id": 4},
    6: {"student_id": 6, "course_id": 4}
}

student_post_args = reqparse.RequestParser()
student_post_args.add_argument("first_name", type=str)
student_post_args.add_argument("last_name", type=str)
student_post_args.add_argument("group_id", type=int)

student_course_post_args = reqparse.RequestParser()
student_course_post_args.add_argument("student_id", type=int)
student_course_post_args.add_argument("course_id", type=int)


class SmallerAndEqualGroups(Resource):  # temporary return all groups without count <=17 specification
    def get(self):
        return GROUPS


class AllAssociations(Resource):  # added to see the progress
    def get(self):
        return ASSOCIATIONS


class CourseStudents(Resource):    # temporary return all students without course_name specification
    def get(self, course_name):
        return STUDENTS


class NewStudent(Resource):
    def get(self, student_id):        # GET method added to see the progress
        return STUDENTS[student_id]

    def post(self, student_id):
        args = student_post_args.parse_args()
        if student_id in STUDENTS:
            abort(409, description="A student with that ID already exists.")
        STUDENTS[student_id] = {"first_name": args["first_name"], "last_name": args["last_name"],
                                "group_id": args["group_id"]}
        return STUDENTS[student_id]


class DeleteStudent(Resource):
    def delete(self, student_id):
        del STUDENTS[student_id]
        return STUDENTS


class AddStudentToCourse(Resource):
    def post(self, student_id, course_id):
        args = student_course_post_args.parse_args()
        if student_id not in ASSOCIATIONS:
            abort(404, description="Student doesn't exist, cannot update.")
        ASSOCIATIONS[student_id] = {"student_id": args["student_id"], "course_id": args["course_id"]}
        return ASSOCIATIONS[student_id]


class RemoveStudentFromCourse(Resource):    # temporary return all students without course_name specification
    def delete(self, student_id):
        del ASSOCIATIONS[student_id]
        return ASSOCIATIONS


api.add_resource(SmallerAndEqualGroups, "/api/v1/groups/students")
api.add_resource(AllAssociations, "/api/v1/associations")
api.add_resource(CourseStudents, "/api/v1/courses/<string:course_name>/students")
api.add_resource(NewStudent, "/api/v1/students/<int:student_id>")
api.add_resource(DeleteStudent, "/api/v1/students/<int:student_id>")
api.add_resource(AddStudentToCourse, "/api/v1/students/<int:student_id>/<int:course_id>")
api.add_resource(RemoveStudentFromCourse, "/api/v1/students/<int:student_id>")


if __name__ == "__main__":
    app.run(debug=True)


