from faker import Faker

fake = Faker()


def create_student_first_name(number):
    first_names = []
    for _ in range(0, number):
        first_names.append(fake.first_name())
    return first_names


def create_student_last_name(number):
    last_names = []
    for _ in range(0, number):
        last_names.append(fake.last_name())
    return last_names


def create_description(number):
    descriptions = []
    for _ in range(0, number):
        descriptions.append(fake.sentence(nb_words=10, variable_nb_words=False))
    return descriptions


def create_course_name(number):
    course_names = []
    for _ in range(0, number):
        course_names.append(fake.word())
    return course_names


group_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
group_names = ["AA-01", "AA-02", "AA-03", "AA-04", "AA-05", "AA-06", "AA-07", "AA-08", "AA-09", "AA-10"]


print(create_student_first_name(10))  # names

print(create_student_last_name(10))   # surnames

print(create_description(10))         # descriptions to courses

print(create_course_name(10))         # courses' names

print(group_ids)                      # groups' ids

print(group_names)                    # groups' names

